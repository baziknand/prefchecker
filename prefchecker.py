#!/usr/bin/python3

import requests
import subprocess
import datetime
import time

session = requests.Session()
session.headers.update({'User-Agent': 'Mozilla/5.0'})
rdvtype_form = {"planning": "31859", "nextButton":"Etape suivante"}

date = datetime.datetime
log_file = "connections.log"

wait_time = 5


def write_file(line):
    with open(log_file, "a") as ff:
        ff.write(line)
    


while True:
    resp = None
    # log request
    write_file(f"submitting request : {date.now()}\n")
    try:
        resp = session.post("http://www.essonne.gouv.fr/booking/create/31858/1", data = rdvtype_form)
        write_file(f"{resp.status_code} : {date.now()}\n")
    except Exception as e:
        write_file(f"{e} : {date.now()}\n")
        

    # check request status
    if(resp):
        write_file(f"request succeeded : {date.now()}\n")
        # no open sessions, do nothing
        if "plus de plage" in resp.text:
            write_file(f"still no time : {date.now()}\n")
            wait_time = 5 
        else: # open sessions, send sms
            write_file(f">>>> open sessions ! : {date.now()}\n")
            cmd = ["bash", "-c",
                    'aws sns publish --topic-arn arn:aws:sns:eu-central-1:189385494274:prefchecker --message "créneaux ouverts à la prefecture"']
            subprocess.check_output(cmd)
            wait_time = 120 

    else:
        write_file(f"request failed ! : {date.now()}\n")

    write_file(f"sleeping ... : {date.now()}\n")
    time.sleep(wait_time * 60) 
